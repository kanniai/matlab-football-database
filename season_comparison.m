function season_comparison(in)
% A function to calculate paired statistics and plot them to express the
% relationship between chosen variables
% Input:
%   - The given season
%   - The given variable pair
%   - GUI Handles

season_idx = in{1}; stat_idx = in{2}; handles = in{3};

%% Initialize the GUI
handles.text2.Visible = 'off';
plot(0,0, 'Parent', handles.axes1)

%% The pop-up-menu selection can't be 1 (since it's the header of the menu) -> printout an error
if season_idx == 1 || stat_idx == 1
    handles.axes1.Visible = 'off';
    handles.text2.Visible = 'on';
    return
end

idx = season_idx - 1; % Remove the padding of the togglebutton
%% Read the data
season = read_data('data\*csv');
season_table = season{idx};
table_size = size(season_table);

%% Initialize the database variables
teams = unique(season_table{:,1});
data_x = zeros(20,1); data_y = zeros(20,1);

switch stat_idx % Based on the user's choice, choose the statistics to show
    case 2
        data_x_idx = 4; data_y_idx = 2; xl = 'Shots'; yl = 'Goals';
    case 3
        data_x_idx = 2; data_y_idx = 1; xl = 'Goals'; yl = 'Points';
    case 4
        data_x_idx = 5; data_y_idx = 6; xl = 'Fouls'; yl = 'Yellow cards';
    case 5
        data_x_idx = 5; data_y_idx = 7; xl = 'Fouls'; yl = 'Red cards';
end

%% Fill the data
for i = 1:table_size(1)
    matchdata = match(season_table(i,:));
    for j = 1:2 % Home and away matches for the given team
        idx = find(strcmp(teams, matchdata{j}.team));
        data_x(idx) = data_x(idx) + matchdata{j}.data(data_x_idx);
        data_y(idx) = data_y(idx) + matchdata{j}.data(data_y_idx);   
    end
end

clrs = rand(20,3); % Select randomly the 20 different colors
for k=1:length(data_x)   
    scatter(data_x(k), data_y(k), 30, clrs(k,:), 'o', 'filled', 'Displayname', teams{k}); % Plot the data
    hold on
end
xlabel(xl, 'Parent', handles.axes1)
ylabel(yl, 'Parent', handles.axes1)
legend('Location', 'northoutside', 'NumColumns', 4, 'FontSize', 6) % Legend outside of the plot
legend show
hold off
