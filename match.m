function out = match(in)
% A function, which takes a row (match) of the database, and calculates the
% data
% Input:
%   - cell row of match data
% Output:
%   - extracted data from the match for both home and away teams:
%   calculate the winner, given points, goals, shots etc.

row = 1;
pts_h = 0; pts_a = 0;

%% Find out the winner and allocate points
result = in{row, 5};
if char(result) == 'H'
    pts_h = 3;
elseif char(result) == 'A'
    pts_a = 3;
else
    pts_h = 1; pts_a = 1;
end

% Output: 'team' ; points, goals for, goals against, shots, fouls, yellows, reds
home = struct('team', in{row,1}, 'data', [pts_h, in{row,3}, in{row,4}, in{row,6}, in{row,8}, in{row,10}, in{row,12}]);
away = struct('team', in{row,2}, 'data', [pts_a, in{row,4}, in{row,3}, in{row,7}, in{row,9}, in{row,11}, in{row,13}]);

out = {home, away};

