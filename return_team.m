function out = return_team(idx)
% A function which returns the team name from the given index
% Input:
%   - An index corresponding to the team in alphabetical order
% Output:
%   - The corresponding team
team_list = {'Alaves ','Almeria ','Ath Bilbao ','Ath Madrid ','Barcelona ','Betis ','Cadiz ','Celta ','Cordoba ','Eibar ','Elche ','Espanol ','Getafe ','Gimnastic ','Girona ','Granada ','Hercules ','Huesca ','La Coruna ','Las Palmas ','Leganes ','Levante ','Malaga ','Mallorca ','Murcia ','Numancia ','Osasuna ','Real Madrid ','Recreativo ','Santander ','Sevilla ','Sociedad ','Sp Gijon ','Tenerife ','Valencia ','Valladolid ','Vallecano ','Villarreal ','Xerez '};
indices = 2:length(team_list);

team_idx = indices==idx;
out = team_list(team_idx);
end
