function out = read_data(path)
% read_data function
% Read data from the given path
% Input:
%   - path of the data
% Output:
%   - exctracted data in cell array form
warning('off','all')

files = dir(path);
database = {};

vars= ["HomeTeam", "AwayTeam", "FTHG", "FTAG", "FTR", "HS", "AS", "HF", "AF", "HY", "AY", "HR", "AR"];

%% Choose the selected columns from the database (not using all columns)
for i = 1:length(files)
    data = readtable(files(i).name);
    data = data(:, data.Properties.VariableNames(vars));
    database{end+1} = data;
end

out = database;





