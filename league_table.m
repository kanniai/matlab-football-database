function out = league_table(in)
% Print out the league table using the GUI environment
% Additionally return the league results of the given year
% Input:
%   - Index of the given season in the database
%   - GUI handles
% Output:
%   - The order of the teams according to the final standings of the given
%   season

in_idx = in{1}; stat = in{2}; handles = in{3};
temp_handles = handles;
handles.text2.Visible = 'off';

%% The pop-up-menu selection can't be 1 (since it's the header of the menu) -> printout an error
if in_idx == 1
    handles.uitable1.Visible = 'off';
    handles.text2.Visible = 'on';
    return
end

%% Find the given season from the database
idx = in_idx - 1; % Remove the padding of the togglebutton
season = read_data('data\*csv');
season_table = season{idx};
table_size = size(season_table);

%% Initialize the database variables
teams = unique(season_table{:,1});
points = zeros(20,1); home_points = zeros(20,1); away_points = zeros(20,1);
ga = zeros(20,1); gf = zeros(20,1);

%% Fill the data
for i = 1:table_size(1)
    matchdata = match(season_table(i,:));
    for j = 1:2 
        idx = find(strcmp(teams, matchdata{j}.team));
        switch j
            case 1; home_points(idx) = home_points(idx) + matchdata{j}.data(1);
            case 2; away_points(idx) = away_points(idx) + matchdata{j}.data(1);
        end
        points(idx) = points(idx) + matchdata{j}.data(1);
        gf(idx) = gf(idx) + matchdata{j}.data(2);
        ga(idx) = ga(idx) + matchdata{j}.data(3);
    end
end

%% Prepare the table and sort based on the league points
gf = num2cell(gf); ga = num2cell(ga); points = num2cell(points);
home_points = num2cell(home_points); away_points = num2cell(away_points);
matches = num2cell(table_size(1)/10.*ones(20,1));
C = [teams, matches, points, home_points, away_points, gf, ga];
sort = stat + 2; % padding of 2 for teams and matches
C = sortrows(C, sort, 'descend');

%% Either return the table for another function, or printout the final table to the GUI
if isempty(temp_handles)
    out = C;
    return
else
    t = handles.uitable1;
    handles.uitable1.Visible = 'on';
    set(t, 'Data', C, 'ColumnName', {'Team name', 'Matches', 'Points', 'Home', 'Away', 'GF', 'GA'})
end


