This is the README file for the MATLAB project 'Football Data'
Author: Matias Kanniainen
MATLAB version: R2021b, 9.11.0.1837725
--------------------------------------------------------------

Start the program excecution from the 'main_gui.m' function, and
make sure, that all the additional functions including the GUI
.fig-files are in the same path. Also, make sure that the folder
data/ is included in the path, since it is essential to the
funcionality.

The functions work by choosing the parameters from the popupmenus.
If the choice is erroneous, the program will the user let know about
it. In that case, please select another variable from the menu.

To close the program, make sure to close all the GUIs with the
'close' button, or the 'x' in the top right corner.