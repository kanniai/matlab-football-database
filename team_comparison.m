function team_comparison(in)
% A function to compare the history of results of two teams
% Plots a bar plot to illustrate the results
% Input:
%   - The home team index
%   - The away team index
%   - GUI handles

home_idx = in{1}; away_idx = in{2}; handles = in{3};

home_team = return_team(home_idx);  away_team = return_team(away_idx); % Get the corresponding teams

%% Initialize the GUI and show an error message in case of wrong input
handles.text4.Visible = 'off';
handles.text8.Visible = 'off';
handles.axes1.Visible = 'on';
plot(0,0, 'Parent', handles.axes1)
if home_idx == away_idx || home_idx == 1 || away_idx == 1
    handles.axes1.Visible = 'off';
    handles.text4.Visible = 'on';
    return
end

%% Read the data
seasons = read_data('data\*csv');
home_wins = 0; away_wins = 0; ties = 0;
for i=1:length(seasons) % Go through all the seasons
    season = seasons{i};
    table_size = size(season);

    for j=1:table_size(1)
        if strcmp(season{j,1}, strip(home_team)) & strcmp(season{j,2}, strip(away_team)) % Find the matches between given teams
            if strcmp(season{j,5}, 'H'); home_wins = home_wins+1; % Home team win
            elseif strcmp(season{j,5},'A'); away_wins = away_wins+1; % Away team win
            elseif strcmp(season{j,5}, 'D'); ties = ties+1; % Tie
            end
        else
            continue
        end
    end
end

total = home_wins+away_wins+ties;

if total == 0
    handles.axes1.Visible = 'off';
    handles.text8.Visible = 'on';
    return
end

%% Plot the results
str = "Total matches: "+total;
bar_data = [home_wins away_wins ties];
labels = {string(home_team), string(away_team), "Tie"};
bar(bar_data, 'Parent', handles.axes1)
set(handles.axes1, 'XTick', 1:numel(labels), 'XTickLabel', labels)
title(handles.axes1, str);
