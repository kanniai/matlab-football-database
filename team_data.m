function team_data(in)
% A function to show the results of a single team with different settings
% in the GUI environment by plotting the data
% Input:
%   - Index corresponding to the given team
%   - The given season
%   - Chosen extra statistic
%   - GUI handles

team_idx = in{1}; season_idx = in{2}; stat_idx = in{3}; handles = in{4};

%% Initialize the gui
handles.text4.Visible = 'off';
handles.text3.Visible = 'off';
handles.axes1.Visible = 'on';
handles.axes2.Visible = 'on';
plot(0,0, 'Parent', handles.axes1)
plot(0,0, 'Parent', handles.axes2)

%% Printout the error messages if the selection of the variables is not correct
if team_idx == 1 || season_idx == 1 || stat_idx == 1
    handles.text4.Visible = 'on';
    handles.text3.Visible = 'off';
    handles.axes1.Visible = 'off';
    handles.axes2.Visible = 'off';
    return
end

if season_idx == 2 %% If the user has chosen option 'overall', which combines the data from every season in the database
    overall_data = overall({team_idx, stat_idx});
    plot_points = overall_data(:,1);
    plot_data = overall_data(:,2);

    %% Initialize the axis and the labels
    pointslabel = "Position";
    x = 1:length(plot_data);
    x_ticks = 1:length(plot_data);
    y_ticks = 1:20;
    y_ticklabels = ["1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20"];
    x_ticklabels = ["05-06" "06-07" "07-08" "08-09" "09-10" "10-11" "11-12" "12-13" "13-14" "14-15" "15-16" "16-17" "17-18" "18-19" "19-20" "20-21" "21-22"];
    x_label="Season";
    direction = 'reverse';
else %% If the user selects a single season
    season = read_data('data\*csv');
    season_table = season{season_idx-2};
    table_size = size(season_table);  
    
    team = return_team(team_idx);
    
    %% If the given team has not played in the given season, display error
    if strcmp(unique(season_table{:,1}), strip(team)) == zeros(20,1)
        handles.text3.Visible = 'on';
        handles.text4.Visible = 'off';
        handles.axes1.Visible = 'off';
        handles.axes2.Visible = 'off';
        return
    end
    
    %% Collect the statistics for the given team from the given season
    plot_data = []; points = [];
    for i=1:table_size(1)
        if strcmp(season_table{i,1}, strip(team)) % home team
            idx = 1;
        elseif strcmp(season_table{i,2}, strip(team)) % away team
            idx = 2;
        else
            continue
        end
        %% Get the data from the matches
        matchdata = match(season_table(i,:));
        points(end+1) = matchdata{idx}.data(1);
        plot_data(end+1) = matchdata{idx}.data(stat_idx);
    end
    
    %% Initialize the data, and the labels for the axis
    plot_points = cumsum(points);
    pointslabel = "Points";
    x_label = "Matches";
    x = 1:length(plot_points);
    y_ticks = 0:20:100;
    y_ticklabels = ["0","20", "40", "60", "80", "100"];
    x_ticks = [1, 10, 20, 30, 38];
    x_ticklabels = ["1", "10", "20", "30", "38"];
    direction = 'normal';

end

%% Plot the results
label_array = {'Goals scored', 'Goals conceded', 'Shots', 'Fouls'};
plot(x, plot_points, 'Parent', handles.axes1) % Plot the points/final standings
xlabel(x_label, 'Parent', handles.axes1)
ylabel(pointslabel, 'Parent', handles.axes1)
set(handles.axes1, 'YDir', direction);
handles.axes1.YTick = y_ticks;
handles.axes1.XTick = x_ticks;
handles.axes1.YTickLabel = y_ticklabels;
handles.axes1.XTickLabel = x_ticklabels;

plot(x, plot_data, 'Parent', handles.axes2) % Plot the additional stat
xlabel(x_label, 'Parent', handles.axes2)
ylabel(label_array{stat_idx-1}, 'Parent', handles.axes2)
handles.axes2.XTick = x_ticks;
handles.axes2.XTickLabel = x_ticklabels;
