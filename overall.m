function out = overall(in)
% A function to calculate the statistics of one team across multiple
% seasons
% Input:
%   - The team index
%   - The given statistic
% Output:
%   - The standings and wanted statistic from across the database
% NOTE! Due to the large amount of data, the processing of this function
% takes some time, up to 20 seconds in my computer

idx = in{1}; stat_idx = in{2};
team = return_team(idx);

seasons = read_data('data\*csv');

results = []; stat = [];
for i=1:length(seasons) % Go through all the seasons
    season = seasons{i};
    table_size = size(season);
    temp_stat = [];
    scoreboard = league_table({i+1, 1 []}); % Call the scoreboard of each season based on points
    scoreboard = scoreboard(:,1);
    for j=1:table_size(1)
        if strcmp(season{j,1}, strip(team)) % home team
            idx = 1;
        elseif strcmp(season{j,2}, strip(team)) % away team
            idx = 2;
        else
            continue
        end
        %% Find the additional statistics from the seasons
        matchdata = match(season(j,:));
        temp_stat(end+1) = matchdata{idx}.data(stat_idx);
    end
    if any(strcmp(scoreboard, strip(team))) % If the team has played in the season
        position = find(strcmp(scoreboard, strip(team))); % Find the final standing from the league table
    else % if the team hasn't played in the league in the season
        position = 21; % Give the last position
    end
    %% Sum up all the seasons
    results(end+1) = position;
    stat(end+1) = sum(temp_stat);
end

out = [flip(results(:)) stat(:)];
