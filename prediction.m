function prediction(in)
% A function to calculate a prediction of the final standing given the
% points during the seasons, or the points of the season given the final
% standing
% Input:
%   - The value of the points/standing
%   - The choice whether the value to be calculated is the points/standing
%   - GUI handles

value = in{1}; choice = in{2}; handles = in{3};

%% Initialize the GUI
handles.text2.Visible = 'off';
handles.text3.Visible = 'off';
handles.text4.Visible = 'off';
plot(0,0, 'Parent', handles.axes1)

seasons = read_data('data\*csv');

%% Exceptions
if floor(value) ~= value % Check that input is int
    handles.axes1.Visible = 'off';
    handles.text4.Visible = 'on';
    return
end

if choice == 1 & ((20 < value) || (value < 1)) % Position must be 20 < position < 1
    handles.axes1.Visible = 'off';
    handles.text3.Visible = 'on';
    return
end
handles.axes1.Visible = 'on';

%% Calculate the data
points = []; standing = [];
for i=1:length(seasons) % Go through all the seasons
    scoreboard = league_table({i+1, 1, []}); % Call the scoreboard of each season based on points
    for j=1:length(scoreboard)
        points(end+1) = scoreboard{j,3};
        standing(end+1) = j;
    end
end

[points,idx]=sort(points,'descend'); % Sort the points by standings
standing=standing(idx);

%% Plotting of the data
scatter(points, standing, 'Parent', handles.axes1, 10, 'blue', 'filled') % Plot all the results
hold on
set(handles.axes1, 'YDir', 'reverse');
xlabel('Points', 'Parent', handles.axes1)
ylabel('Final standing', 'Parent', handles.axes1)
ylim([1,20])
handles.axes1.YTick = 1:5:20;
handles.text2.Visible = 'on';

%% The linear regression
p = polyfit(points, standing, 1); % Create the linear fit
r = p(1).*points + p(2); % Calculate regression coefficients
plot(points, r, 'red', 'LineWidth', 1);

if choice == 1
    y_pred = value;
    x_pred = (y_pred - p(2))/(p(1));
    txt = "To achieve the standing "+value+", usually "+floor(x_pred)+" points are needed.";
else
    x_pred = value;
    y_pred = p(1)*x_pred + p(2);
    if 20 < y_pred; y_pred = 20;
    elseif 1 > y_pred; y_pred = 1;
    end
    txt = "With "+value+" points, the estimated final standing is "+floor(y_pred)+".";
end

%% Plotting of the prediction
scatter(x_pred, y_pred, 'green', 'filled') % Plot the prediction
legend({'Data points', 'Linear fit', 'Prediction'}, 'Location', 'southeast')
set(handles.text2, 'String', txt) % Set the results to text label in GUI
hold off
